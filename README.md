# DWM
## Patches
### Patches
  - [Restartsig](https://dwm.suckless.org/patches/restartsig/)
  - [Stacker](https://dwm.suckless.org/patches/stacker/)
  - [Scratchpad](https://dwm.suckless.org/patches/scratchpad/)
  - [Actualfullscreen](https://dwm.suckless.org/patches/actualfullscreen/)
  - [Sticky](https://dwm.suckless.org/patches/sticky/)
  - [Vanitygaps](https://dwm.suckless.org/patches/vanitygaps/)
  - [Swallow](https://dwm.suckless.org/patches/swallow/)
	- [Systray](https://dwm.suckless.org/patches/systray/)
	- [Dwmblocks systray patch](https://github.com/ashish-yadav11/dwmblocks/tree/master/patches)
## Added layouts
  - [Fibonacci: spiral and dwindle](https://dwm.suckless.org/patches/fibonacci/)
  - [Deck](https://dwm.suckless.org/patches/deck/)
  - [Centeredmaster and centeredfloatingmaster](https://dwm.suckless.org/patches/centeredmaster/)
